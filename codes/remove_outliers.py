def remove_outliers(data, max_deviation=3.5):
    """
    Project: HiCExplorer
    Author: deeptools   File: utilities.py
    License: GNU General Public License v3.0

    The method is based on the median absolute deviation. See
    Boris Iglewicz and David Hoaglin (1993),
    "Volume 16: How to Detect and Handle Outliers",
    The ASQC Basic References in Quality Control:
    Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    returns the list, without the outliers
    """
    data_temp = data
    median = np.median(data['sensor_value'])
    b_value = 1.4826  # value for normal distribution
    mad = b_value * np.median(np.abs(data['sensor_value'] - median))

    if mad > 0:
        data['deviation'] = abs(data['sensor_value'] - median) / mad
        """
        outliers = data[deviation > max_deviation]
        print "outliers removed {}".format(len(outliers))
        print outliers
        """
        data = data[data['deviation'] <= max_deviation]
    return data

def dataFrameNoOutliers(data, max_deviation):
  """
  Remove all outliers of dataframe
  """
  data_new = pd.DataFrame()
  for waterLevelValue in range(50,400,50):
    for angleValue in [0, 2.5, 5, 7.5, 10]:
      for turbidityValue in ['low', 'mid', 'high']:
        data_new=data_new.append(remove_outliers(data[(data['water_level'] == waterLevelValue)
                                                        & (data['turbidity'] == turbidityValue)
                                                        & (data['angle'] == angleValue)
                                                      ]
                                                , max_deviation
                                                )
                                  ,ignore_index=True
                                )
  return data_new
